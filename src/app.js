const express = require('express');

const rotas = require('./routes.js');

const server = express();

server.use(rotas);

server.listen(3000);

module.exports = server;