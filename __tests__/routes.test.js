const request = require('supertest');
const app = require('../src/app.js');

let server, agent;

beforeEach((done) => {
    console.log('Iniciando os teste');
    server = app.listen(3000, (err) => {
        if (err) return done(err);

        agent = request.agent(server);
        done();
    });
});

afterEach((done) => {
    console.log('Encerrando os teste e fechando o servidor');
    return server && server.close(done);
});

describe('Inicio dos testes', () => {
    test('Verifico se é igual a ok true da resposta', async () => {
        const res = await request(server).get('/');
        expect(res.body).toEqual({ ok: true });
    });

    test('Vejo se o código da requisição é de erro', async () => {
        const res = await request(server).get('/teste');
        expect(res.status).not.toEqual(200);
    });
});